module.exports = {
  content: [
    "./content/*.*",
    "*.php"
  ],
  theme: {
    extend: {
      colors: {
        'provence-orange': '#FF8600',
        'provence-blue': '#5B96F8',
        'provence-violin': '#967BB6',
      },
      typography: {
        DEFAULT: {
          css: {
            color: '#2f2f23',
            a: {
              color: '#2f2f23',
              textDecorationColor: '#FF8600',
              '&:hover': {
                color: '#FF8600',
              },
            },
            'h1,h2,h3': {
              color: '#FF8600',
              lineHeight: 1.2,
              fontWeight: 400,
            },
            'h1,h2,h3,p,ul,ol,table,blockquote,figure': {
              marginTop: 0,
              marginBottom: '1.75rem',
            },
            'blockquote': {
              marginBottom: 0,
            }
          },
        },
        lg: {
          css: {
            'h1,h2,h3': {
              lineHeight: 1.2,
            },
            'h1,h2,h3,p,ul,ol,table,blockquote,figure': {
              marginTop: 0,
              marginBottom: '1.75rem',
            },
            'blockquote': {
              marginBottom: 0,
            }
          }
        }
      },
    },
    container: {
      center: true,
    },
  },
  plugins: [
    require('@tailwindcss/typography')
  ],
}
