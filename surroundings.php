<?php

require_once '_init.inc.php';

$title = __('Okolí Apartmánu');
$description = __('Vítejte v Drôme Provençale, hornaté oblasti mezi řekou Rhônou a&nbsp;Alpami.');
$image = '/images/2022-opt/okoli-crest-vyhlidka-2.jpeg';

require_once '_head.inc.php';

?>

<div class="container home">

      <div class="nav" role="navigation">
        <h2 class="sr-only">Navigace</h2>
        <ul class="nav__wrapper">
          <li class="nav__item">
            <a class="nav__item-inside" href="/"><?php echo __('Úvod')  ?></a>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="accommodation.php"><?php echo __('Ubytování')  ?></a>
          </li>
          <li class="nav__item">
            <span class="nav__item-inside"><?php echo __('Okolí')  ?></span>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="contact.php"><?php echo __('Kontakt')  ?></a>
          </li>
        </ul>
      </div><!-- #nav -->

    <div class="content-container">

    <?php
    require_once '_offer_bar.inc.php';
    ?>

    <div class="content">

      <!-- Uvod -->

      <div id="location" class="content__heading">
        <?php require_once('content/text_surroundings_heading_'.$lang.'.inc.php');  ?>
      </div>
      <div class="content__photos">
        <?php require_once('content/photos_surroundings.inc.php');  ?>
      </div>
      <div class="content__text">
        <?php require_once('content/text_surroundings_'.$lang.'.inc.php');  ?>
      </div>

      <!-- Zajimava mista -->

      <div id="places" class="content__heading">
        <?php require_once('content/text_places_heading_'.$lang.'.inc.php');  ?>
      </div>

      <!-- Romanticka mestecka -->

      <div class="content__text">
        <?php require_once('content/text_places-cities_'.$lang.'.inc.php');  ?>
      </div>
      <div class="content__photos">
        <?php require_once('content/photos_cities.inc.php');  ?>
      </div>

      <hr class="divider">

      <!-- Trhy -->

      <div class="content__text">
        <?php require_once('content/text_places-markets_'.$lang.'.inc.php');  ?>
      </div>
      <div class="content__photos">
        <?php require_once('content/photos_markets.inc.php');  ?>
      </div>

      <hr class="divider">

      <!-- Pesi -->

      <div class="content__text">
        <?php require_once('content/text_places-hiking_'.$lang.'.inc.php');  ?>
      </div>
      <div class="content__photos">
        <?php require_once('content/photos_hiking.inc.php');  ?>
      </div>

      <hr class="divider">

      <!-- Voda -->

      <div class="content__text">
        <?php require_once('content/text_places-water_'.$lang.'.inc.php');  ?>
      </div>
      <div class="content__photos">
        <?php require_once('content/photos_water.inc.php');  ?>
      </div>

      <hr class="divider">

      <!-- Vzdalenejsi mesta -->

      <div class="md:grid md:grid-cols-[1fr_3fr] md:gap-8">
          <div class="prose">
            <?php require_once('content/text_places-distant_'.$lang.'.inc.php');  ?>
          </div>
          <div>
          <?php if ($detect->isMobile() && !$detect->isTablet()): ?>
            <p class="center">
              <a class="button" href="https://maps.google.com/maps/ms?msid=217846170008325892290.0004bc13b343593ee2a18&amp;msa=0&amp;ie=UTF8&amp;t=h&amp;ll=44.885066,5.130615&amp;spn=0.807584,1.702881&amp;z=9&amp;iwloc=0004bc13c145d0063541e&amp;source=embed">
                <?php echo __('Mapa okolí na Google Maps') ?>
              </a>
            </p>
            <?php else: ?>
              <div class="map">
                <iframe width="756" height="561" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://maps.google.com/maps/ms?msid=217846170008325892290.0004bc13b343593ee2a18&amp;msa=0&amp;ie=UTF8&amp;t=h&amp;ll=44.885066,5.130615&amp;spn=0.807584,1.702881&amp;z=9&amp;iwloc=0004bc13c145d0063541e&amp;output=embed"></iframe>
              </div><!-- .map-container -->
              <?php if ($lang == 'cz'): ?>
              <p>
                <small>Podívejte se na <a href="https://maps.google.com/maps/ms?msid=217846170008325892290.0004bc13b343593ee2a18&amp;msa=0&amp;ie=UTF8&amp;t=h&amp;ll=44.885066,5.130615&amp;spn=0.807584,1.702881&amp;z=9&amp;iwloc=0004bc13c145d0063541e&amp;source=embed">větší mapu</a></small>
              </p>
              <?php endif; ?>
            <?php endif; ?>
          </div>
        </div>

    </div><!-- .content -->

    </div><!-- .content-container -->

<?php
require_once '_page_foot.inc.php';
?>

</div><!-- .container -->


<?php
require_once '_foot.inc.php';
?>
