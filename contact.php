<?php

require_once '_init.inc.php';

$title = __('Kontakt');
$description = __('Vítejte v Drôme Provençale, hornaté oblasti mezi řekou Rhônou a&nbsp;Alpami.');
$image = '/images/2022-opt/venku-vecer-2.jpeg';

require_once '_head.inc.php';

?>

<div class="container home">

      <div class="nav" role="navigation">
        <h2 class="sr-only">Navigace</h2>
        <ul class="nav__wrapper">
          <li class="nav__item">
            <a class="nav__item-inside" href="/"><?php echo __('Úvod')  ?></a>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="accommodation.php"><?php echo __('Ubytování')  ?></a>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="surroundings.php"><?php echo __('Okolí')  ?></a>
          </li>
          <li class="nav__item">
            <span class="nav__item-inside"><?php echo __('Kontakt')  ?></span>
          </li>
        </ul>
      </div><!-- #nav -->

    <div class="content-container">

    <?php
    require_once '_offer_bar.inc.php';
    ?>

    <div class="content">

      <!-- Uvod -->

      <div id="location" class="content__heading">
        <?php require_once('content/text_contact_heading_'.$lang.'.inc.php');  ?>
      </div>
      <div class="content__photos">
        <?php require_once('content/photos_contact.inc.php');  ?>
      </div>
      <div class="md:grid md:grid-cols-[2fr_1fr] md:gap-8 mb-12">
        <div class="prose">
          <?php require_once('content/text_contact_'.$lang.'.inc.php');  ?>
        </div>
        <div>
          <?php require_once('content/text_contact_box_'.$lang.'.inc.php');  ?>
        </div>
      </div>
      <div class="content__photos mb-12">
        <p class="content__photos-item-full">
          <img src="./images/2022-opt/okoli-puy-shora.webp" class="content__photos-img" loading="lazy" decoding="async"  lightbox="group-contact" alt="">
        </p>
      </div>
      <div class="md:grid md:grid-cols-[2fr_1fr] md:gap-8">
        <div class="prose">
          <?php require_once('content/text_contact_reach_'.$lang.'.inc.php');  ?>
        </div>
      </div>

    </div><!-- .content -->

    </div><!-- .content-container -->

<?php
require_once '_page_foot.inc.php';
?>

</div><!-- .container -->


<?php
require_once '_foot.inc.php';
?>
