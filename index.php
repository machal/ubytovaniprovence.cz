<?php

require_once '_init.inc.php';

$title = __('Aviotte - Ubytování v Provence, Francie');
$description = __('Apartmán Aviotte. Vaše klidná dovolená mezi levandulovými&nbsp;poli.');
$image = '/images/2022-opt/venku-komplet.jpeg';

require_once '_head.inc.php';

?>

  <div class="container home">

      <div class="nav" role="navigation">
        <h2 class="sr-only">Navigace</h2>
        <ul class="nav__wrapper">
          <li class="nav__item">
            <span class="nav__item-inside"><?php echo __('Úvod')  ?></span>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="accommodation.php"><?php echo __('Ubytování')  ?></a>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="surroundings.php"><?php echo __('Okolí')  ?></a>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="contact.php"><?php echo __('Kontakt')  ?></a>
          </li>
        </ul>
      </div><!-- #nav -->

    <div class="content-container">

      <?php
        require_once '_offer_bar.inc.php';
      ?>

      <div class="content">

        <!-- Home -->

        <div id="apartment" class="content__heading">
          <?php require_once('content/text_home_heading_'.$lang.'.inc.php');  ?>
        </div>

        <div class="content__photos">
          <?php require_once('content/photos_home.inc.php');  ?>
        </div>

        <div class="content__text">
          <?php require_once('content/text_home_'.$lang.'.inc.php');  ?>
        </div>

        <div class="mb-12">
          <div style="padding:56.25% 0 0 0;position:relative;">
            <iframe loading="lazy" src="https://player.vimeo.com/video/652915534?h=0f209e64ac&amp;badge=0&amp;autopause=0&amp;player_id=0&amp;app_id=58479" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen style="position:absolute;top:0;left:0;width:100%;height:100%;" title="Penzion Aviotte (1)"></iframe>
          </div>
          <script async src="https://player.vimeo.com/api/player.js"></script>
        </div>

        <div class="md:grid md:grid-cols-[2fr_1fr] md:gap-8">
          <div class="prose">
            <?php require_once('content/text_testimonials_'.$lang.'.inc.php'); ?>
          </div>
          <div class="prose">
            <?php require_once('content/text_warning_'.$lang.'.inc.php'); ?>
          </div>
        </div>

      </div><!-- .content -->

      </div><!-- .content-container -->

<?php
require_once '_page_foot.inc.php';
?>

  </div><!-- .container -->

<?php
require_once '_foot.inc.php';
?>
