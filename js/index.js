
// 1) Jen velke displeje
// 2) Helpery

(function(){

  var
    ie_version = getInternetExplorerVersion(),
    big_screen_or_legacy_ie = Modernizr.mq('screen and (min-width: 640px)') || (ie_version == 8);

  // --- 1) Jen velke displeje ---
  // Fancybox a iOS slider

  Modernizr.load({

    test: big_screen_or_legacy_ie,

    yep : [
		  'js/jquery.iosslider.js',
  		'js/jquery.fancybox.pack.js',
  	],

    complete : function () {

  		 if (big_screen_or_legacy_ie) {

  		   // Fancybox

          $(".photos a, .fancybox, .gallery-small a").fancybox();

         	$("#obsazenost, .iframe").fancybox({
         		'hideOnContentClick': false
         	});

          // iOS slider

          function slideChange(args) {
          	$('.slideSelectors .item').removeClass('selected');
          	$('.slideSelectors .item:eq(' + (args.currentSlideNumber - 1) + ')').addClass('selected');
          }

          $('.iosSlider').iosSlider({
        		desktopClickDrag: true,
        		snapToChildren: true,
        		navSlideSelector: '.slideSelectors .item',
        		onSlideChange: slideChange,
        		scrollbar: false,
        		keyboardControls: true
        	});

        }  // if (big_screen_or_legacy_ie)

     }	// complete

  });


  // --- 2) Helpery ---

  // Vraci verzi IE
  // http://blogs.msdn.com/b/giorgio/archive/2009/04/14/how-to-detect-ie8-using-javascript-client-side.aspx
  function getInternetExplorerVersion() {
      var rv = -1; // Return value assumes failure.
      if (navigator.appName == 'Microsoft Internet Explorer') {
          var ua = navigator.userAgent;
          var re = new RegExp("MSIE ([0-9]{1,}[\.0-9]{0,})");
          if (re.exec(ua) != null)
              rv = parseFloat(RegExp.$1);
      }
      return rv;
  }

})();
