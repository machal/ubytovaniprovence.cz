<h2>More distant cities</h2>

<p>Get in the car and take a trip. Behind history to Avignon or across the sea to Marseille. </p>

<ul>
   <li><a href="https://cs.wikipedia.org/wiki/Orange_(France)">Orange</a> (45 minutes by car)</li>
   <li><a href="https://cs.wikipedia.org/wiki/Pont_du_Gard">Pont du Gard</a>, <a href="https://cs.wikipedia.org/wiki/Avignon" >Avignon</a> (hour)</li>
   <li><a href="https://cs.wikipedia.org/wiki/Arles">Arles</a> (1.5 hours)</li>
   <li><a href="https://cs.wikipedia.org/wiki/Marseille">Marseille</a> and the sea (2 hours) </li>
</ul>
