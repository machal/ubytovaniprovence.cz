<h1>
Accommodation Aviotte. Your
peaceful holiday in the middle of Provence and the Alps.
</h1>

<p>
We offer several types of <a href="/accommodation.php">accommodation</a> in sunny south-eastern France.
  Choose from a spacious apartment for five, a chalet for four or a room for two.
</p>
