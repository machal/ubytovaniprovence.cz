<h3>Pěší turistika</h3>

<p>V oblasti Auvergne-Rhône-Alpes jste pod horami. Náročnost tras, na které se vydáte, si můžete vybrat – od procházek s dětmi až po výlety jako na českou Sněžku. Na výhledy na barevná pole budete dlouho vzpomínat.</p>

<ul>
  <li>Procházky okolím, mezi poli, farmami a městečky.</li>
<li><a href="https://www.ladrometourisme.com/en/cards/foret-de-saou/">Forêt de Saou</a> (15 minut autem) - trasy nížinou i pro děti, ale také pořádné výšlapy na hory vysoké přes 1 000 metrů. Viz např. Grande-Pomerole.</li>
<li><a href="https://www.ladrometourisme.com/en/cards/cirque-darchiane/">Cirque d&#39;Archiane</a> (1,5 hodiny) - krásné hory v obloukovitém tvaru.</li>
  <li>Pokud raději lezete po skalách, je to možné kousek od vesnice Soau.  </li>
</ul>
