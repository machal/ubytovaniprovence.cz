<div class="column span-1">

  <h1>Facebook</h1>

  <div class="fb-page" data-href="https://www.facebook.com/AviotteProvence/"
    data-tabs="timeline" data-width="300" data-height="420"
    data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="false">
    <div class="fb-xfbml-parse-ignore">
      <blockquote cite="https://www.facebook.com/AviotteProvence/">
        <a href="https://www.facebook.com/AviotteProvence/">Accommodation Aviotte Provence</a>
      </blockquote>
    </div>
  </div>

</div><!-- .column span-1 -->

<div class="column span-1">

  <h1>Kontakt</h1>

  <p>
  Jean-Pierre Aviotte &amp; Zuzana Bilkova
  </p>

  <p>
  <strong>We speak english:</strong>
  <br><a href="mailto:zuzanaviotte@gmail.com">zuzanaviotte@gmail.com</a>
  <br>T 0033 647 562 355
  </p>

  <p>
  <strong>Address:</strong><br>
  535 Chemin De Saudon<br>
  26400 Soyans<br>
  France<br>
  <strong>GPS:</strong>
  N 44°37'37"
  E 4°59'34"<br>
  <a href="https://maps.google.com/maps?f=q&source=s_q&hl=cs&geocode=&q=N+44%C2%B037%2737%22++E+4%C2%B059%2734%22&sll=37.0625,-95.677068&sspn=50.37814,79.101563&ie=UTF8&ll=44.625664,4.993286&spn=2.853962,4.943848&z=8&iwloc=A" title="">
    map
  </a>
  </p>

  <h1>Wie Sie uns erreichen?</h1>

  <h2>Mit dem Auto:</h2>

  <p>
    Wir sind nah an A7 Valence - Marseille.
  </p>

  <h2>Mit dem Flugzeug:</h2>

  <p>
    <a href="https://www.lyonaeroports.com/en/" title="">
      Lyon
    </a>
    und
    <a href="https://www.marseille-airport.com/">
      Marseille
    </a>
    sind ca. 2 Stunden mit dem Auto entfernt.
    Für Auto zu mieten Sie können <a href="https://rentalcars.com">rentalcars.com</a> verwenden.
  </p>


</div><!-- .column span-1 -->

<div class="column span-1 last">

  <h1>Bedingungen</h1>

  <ul>
    <li>Rückkaution 200€ an Ort u.Stelle
    <li>Aufenthalt - Samstag von 13Uhr bis nächsten Samstag 11Uhr.
    <li>Über individuelle Bedingungen können wir im voraus verhandeln.
    <li>Tierchen können zu uns leider nicht kommen.
  </ul>

  <p>
  Von April bis Mai und September bis Oktober sind hier angenehm frühlingshaften Temperaturen um 25° C.
  </p>


</div><!-- .column span-1 -->
