<h2>Zajímavá místa v okolí</h2>

<h3>Romantická městečka</h3>

<p>Ráz kraje dotváří malá, ale nádherná městečka, kde potkáte nejen příjemné lidi, ale skoro v každém něco zajímavého objevíte.</p>

<ul>
  <li><a href="https://www.ladrometourisme.com/en/cards/puy-saint-martin/">Puy-Saint-Martin</a> (5 minut autem) – nejbližší místo, možno zde nakupovat nebo navštívit restaurace.</li>
<li><a href="https://www.ladrometourisme.com/en/cards/saou/">Saou</a> (10 minut) – malebné městečko, vstup do turistické oblasti Forêt.</li>
<li><a href="https://www.ladrometourisme.com/en/cards/crest/">Crest</a> (20 minut autem) – největší z blízkých městeček, určitě vylezte na vyhlídku nad kostelem.</li>
</ul>

<div class="content__photos not-prose">
  <?php require_once('content/photos_cities.inc.php');  ?>
</div>

<h3>Trhy a kultura</h3>

<p>Byť jste tady na venkově a na místě, kde není zase tak moc turistů, poměrně to zde žije. Určitě se zastavte na některých trzích a nasajte jejich neopakovatelnou atmosféru.</p>

<ul>
  <li><a href="https://www.ladrometourisme.com/en/cards/restaurant-la-fontaine-minerale/">La Fontaine Minerale</a> - sousedské slavnosti (neděle)</li>
<li>Restaurace a trhy Puy-Saint-Martin (pondělí)</li>
<li>Trhy v <a href="https://www.ladrometourisme.com/en/cards/restaurant-la-fontaine-minerale/">Bourdeaux</a> (čtvrtek)</li>
</ul>

<div class="content__photos not-prose">
  <?php require_once('content/photos_markets.inc.php');  ?>
</div>

<h3>Pěší turistika</h3>

<p>V oblasti Auvergne-Rhône-Alpes jste pod horami. Náročnost tras, na které se vydáte, si můžete vybrat – od procházek s dětmi až po výlety jako na českou Sněžku. Na výhledy na barevná pole budete dlouho vzpomínat.</p>

<ul>
  <li>Procházky okolím, mezi poli, farmami a městečky.</li>
<li><a href="https://www.ladrometourisme.com/en/cards/foret-de-saou/">Forêt de Saou</a> (15 minut autem) - trasy nížinou i pro děti, ale také pořádné výšlapy na hory vysoké přes 1 000 metrů. Viz např. Grande-Pomerole.</li>
<li><a href="https://www.ladrometourisme.com/en/cards/cirque-darchiane/">Cirque d&#39;Archiane</a> (1,5 hodiny) - krásné hory v obloukovitém tvaru.</li>
  <li>Pokud raději lezete po skalách, je to možné kousek od vesnice Soau.  </li>
</ul>

<div class="content__photos not-prose">
  <?php require_once('content/photos_hiking.inc.php');  ?>
</div>

<h3>Koupání a kanoe</h3>

<p>Moře je sice daleko, ale možností koupání, které v letních dnech určitě oceníte, zde jsou. Řeky, potoky nebo koupaliště na vás čekají.</p>

<ul>
  <li>Potok Le Roubion (5 minut autem) – osvěžení kousek od apartmánu.</li>
<li>Koupaliště <a href="https://www.ladrometourisme.com/en/cards/public-pool-2/">Eyzahut</a> (15 min).</li>
<li>Řeka Dróma (15 minut)  – cachtání pro menší, skákání pro větší, projížďky v proudu.</li>
<li><a href="https://www.ladrometourisme.com/en/cards/canoe-kayak-et-mini-raft-avec-canoe-drome/">Kanoe na řece Dróma</a> –  můžete například lodí z Vercheny do Saillans.  </li>
</ul>

<div class="content__photos not-prose">
<?php require_once('content/photos_water.inc.php');  ?>
</div>

<h3>Vzdálenější města</h3>

<p>Sedněte do auta a udělejte si výlet. Za historií do Avignonu nebo za mořem k Marseille.  </p>

<ul>
  <li><a href="https://cs.wikipedia.org/wiki/Orange_(Francie)">Orange</a> (45 minut autem)</li>
  <li><a href="https://cs.wikipedia.org/wiki/Pont_du_Gard">Pont du Gard</a>, <a href="https://cs.wikipedia.org/wiki/Avignon">Avignon</a> (hodina)</li>
  <li><a href="https://cs.wikipedia.org/wiki/Arles">Arles</a> (1,5 hodiny)</li>
  <li><a href="https://cs.wikipedia.org/wiki/Marseille">Marseille</a> a moře (2 hodiny)  </li>
</ul>
