<h1>Unterkunft Aviotte. Ihr ruhiger Urlaub an den Lavendelfeldern.</h1>

<p>
  Wir bieten Ihnen eine gemütliche Unterkunft im sonnigen Südfrankreich,
  in der Provence, an Sie können durch Drome Provencale mit dem Fahrad
  fahren, die Sehenswürdigkeiten in den alten Städtchen besuchen oder
  mit dem Auto im Nu am Mittelmeer, in den Alpen oder in Avignon sein.
</p>

<h2>
  ⚠️ Informationen zur Saison 2023:
</h2>

<p>
Ab dem 14. März 2022 hat Frankreich die Pflicht zur Vorlage eines Impfpasses für den Zugang zu Dienstleistungen ausgesetzt.
</p>

<p>
Eine Buchung bei uns ist ohne Stornierungsgebühren möglich. Wir sammeln eine Anzahlung von 200 €, die auch für die Saison 2022 gilt.
</p>
