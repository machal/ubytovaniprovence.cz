<h3>Swimming and canoeing</h3>

<p>The sea is far away, but there are swimming opportunities here that you will definitely appreciate on summer days. Rivers, streams or swimming pools are waiting for you.</p>

<ul>
   <li>Le Roubion stream (5 minutes by car) – refreshment not far from the apartment.</li>
     <li>Swimming pool <a href="https://www.ladrometourisme.com/en/cards/public-pool-2/">Eyzahut</a> (15 min).</li>
     <li>Dróma River (15 minutes) – paddling for the little ones, jumping for the big ones, rides in the current.</li>
     <li><a href="https://www.ladrometourisme.com/en/cards/canoe-kayak-et-mini-raft-avec-canoe-drome/">Canoe on the Dróme River</a> - you can for example by boat from Vercheny to Saillans. </li>
</ul>
