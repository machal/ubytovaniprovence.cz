<h2>
   Room equipment
</h2>

<ul>
   <li>140 cm double bed</li>
   <li>bathroom and toilet</li>
   <li>possibility of using common seating for 5-6 people</li>
</ul>


<p>July and August: The room is available with breakfast and without kitchen. </p>

<p>May to September: In combination with an apartment, the room can be used for €350 without breakfast. </p>

<p>An outdoor kitchen is available when ordering a room with a chalet.</p>
