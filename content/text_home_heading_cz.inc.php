<h1>
  Ubytování Aviotte. Vaše
  klidná dovolená mezi Provence a Alpami.
</h1>

<p>
  Nabízíme několik typů <a href="/accommodation.php">ubytování</a> ve slunné jihovýchodní Francii.
  Vyberte si z prostorného apartmánu pro pět lidí, chaletu pro čtyři nebo pokoje pro dvě osoby.
</p>
