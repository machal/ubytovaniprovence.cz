<h3>Koupání a kanoe</h3>

<p>Moře je sice daleko, ale možností koupání, které v letních dnech určitě oceníte, zde jsou. Řeky, potoky nebo koupaliště na vás čekají.</p>

<ul>
  <li>Potok Le Roubion (5 minut autem) – osvěžení kousek od apartmánu.</li>
    <li>Koupaliště <a href="https://www.ladrometourisme.com/en/cards/public-pool-2/">Eyzahut</a> (15 min).</li>
    <li>Řeka Dróma (15 minut)  – cachtání pro menší, skákání pro větší, projížďky v proudu.</li>
    <li><a href="https://www.ladrometourisme.com/en/cards/canoe-kayak-et-mini-raft-avec-canoe-drome/">Kanoe na řece Dróma</a> –  můžete například lodí z Vercheny do Saillans.  </li>
</ul>
