<h2>Jak se k nám dostat?</h2>

<p>
  <strong>Autem</strong>:
  <a href="https://www.google.com/maps/dir/Praha,+%C4%8Cesk%C3%A1+republika/44.6269444,4.9927778/@47.2694573,5.1313579,6z/data=!3m1!4b1!4m9!4m8!1m5!1m1!1s0x470b939c0970798b:0x400af0f66164090!2m2!1d14.4378005!2d50.0755381!1m0!3e0?hl=cs">Z Prahy </a>je to k nám 1 200 km a 12 hodin jízdy. <a href="https://www.google.com/maps/dir/Brno,+%C4%8Cesk%C3%A1+republika/44.6269444,4.9927778/@47.2562225,6.2160349,6z/data=!3m1!4b1!4m9!4m8!1m5!1m1!1s0x4712943ac03f5111:0x400af0f6614b1b0!2m2!1d16.6068371!2d49.1950602!1m0!3e0?hl=cs">Z Brna </a> 1 300 km a 13 hodin jízdy.
</p>

<p>
  <strong>Letecky</strong>: Od dubna do srpna létá 2× týdně společnost <a href="https://www.volotea.com/en/">Volotea </a>z Prahy do Lyonu. Z Marseille pak létá <a href="https://www.google.com/search?q=praha+marseille+ryanair">Ryanair</a>. Na letišti je pak možné si půjčit auto. Z Marseille i z Lyonu jste v Saudon za dvě hodiny.
  Týdenní pronájem vozidla například přes <a href="https://rentalcars.com/">rentalcars.com</a> pro čtyři osoby vyjde přibližně na 150 Euro.
</p>
