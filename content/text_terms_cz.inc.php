<h1>Podmínky ubytování</h1>

<ul>
  <li>Rezervace u nás je možná bez storno poplatků.</li>
  <li>Vratná kauce 200 € na místě. Možno zaplatit v Kč.</li>
  <li>Pobyty jsou od soboty 13:00 do soboty 11:00.</li>
  <li>14denní pobyty máme se slevou.</li>
  <li>Zvířatka k nám bohužel nemohou.</li>
  <li>V ceně ložní prádlo, osušky, ručníky.</li>
  <li>Možnost individuální změny podmínek.</li>
</ul>
