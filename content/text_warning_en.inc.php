<p class="box">
   <strong>Information for the 2023 season:</strong><br>
   Entry to France is now unrestricted due to the Covid-19 pandemic.
   You can find the <a href="https://www.diplomatie.gouv.fr/en/coming-to-france/coming-to-france-your-covid-19-questions-answered/">current status here</a>.
</p>
