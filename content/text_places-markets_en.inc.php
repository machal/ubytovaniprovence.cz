<h3>Markets and Culture</h3>

<p>Even though you are here in the countryside and in a place where there are not so many tourists, it is quite lively here. Be sure to stop at some of the markets and soak up their unique atmosphere.</p>

<ul>
   <li><a href="https://www.ladrometourisme.com/en/cards/restaurant-la-fontaine-minerale/">La Fontaine Minerale</a> - neighborhood festivities (Sunday)</li>
<li>Puy-Saint-Martin restaurants and markets (Monday)</li>
<li>Markets in <a href="https://www.ladrometourisme.com/en/cards/restaurant-la-fontaine-minerale/">Bourdeaux</a> (Thursday)</li>
</ul>
