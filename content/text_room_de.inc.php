
<h1>
	Provenzalischen Zimmer
</h1>

<ul>
  <li>15 m² Zimmer ideal für Paare mit Bad und WC.
	<li>140 cm Doppelbett
	<li>Bad und WC
	<li>Gemeinsamen Outdoor-Küche
</ul>


<h1 class="smaller">
	Gemeinsam Küche, Sitzbereich und Flur für Zimmer
</h1>

<ul>
	<li>Sitzgelegenheiten für 5-6 Personen
	<li>Halboffene als Laube
	<li>Kühlschrank, Gasherd, Mikrowelle, Wasserkocher
</ul>

<p>
  Verfügbar mit Frühstück im Juli und August.
  <br>Kombiniert mit Apartments von Mai bis September für € 350 ohne Frühstück. Die Außenküchen werden dann mit <a href="/chalet.php">Chalet</a> geteilt.
</p>

