<h3>Romantická městečka</h3>

<p>Ráz kraje dotváří malá, ale nádherná městečka, kde potkáte nejen příjemné lidi, ale skoro v každém něco zajímavého objevíte.</p>

<ul>
  <li><a href="https://www.ladrometourisme.com/en/cards/puy-saint-martin/">Puy-Saint-Martin</a> (5 minut autem) – nejbližší místo, možno zde nakupovat nebo navštívit restaurace.</li>
  <li><a href="https://www.ladrometourisme.com/en/cards/saou/">Saou</a> (10 minut) – malebné městečko, vstup do turistické oblasti Forêt.</li>
  <li><a href="https://www.ladrometourisme.com/en/cards/crest/">Crest</a> (20 minut autem) – největší z blízkých městeček, určitě vylezte na vyhlídku nad kostelem.</li>
</ul>
