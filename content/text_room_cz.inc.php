<h2>
  Vybavení pokoje
</h2>

<ul>
  <li>140 cm manželská postel</li>
  <li>koupelna a WC</li>
  <li>možnost využít společné posezení pro 5-6 lidí</li>
</ul>


<p>Červenec a srpen: Pokoj je dostupný se snídaní a bez kuchyně. </p>

<p>Květen až září: V kombinaci s apartmánem je možné pokoj využít za 350 € bez snídaně.  </p>

<p>Venkovní kuchyně je dostupná při objednávce pokoje s chaletem.</p>
