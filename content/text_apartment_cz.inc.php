<h2>
  Apartmán je plně vybavený
</h2>

<ul>
  <li>vlastní kuchyně (lednice, sporák, myčka, kávovar)</li>
  <li>obývací pokoj s knihovnou</li>
  <li>hlavní ložnice s velkou dvoupostelí a postelí pro jednoho</li>
  <li>druhá ložnice s dvoupostelí</li>
  <li>koupelna se sprchovým koutem a samostatné WC</li>
  <li>samostatné venkovní posezení  </li>
</ul>

<p>
  Hlavní ložnice je průchozí, takže ubytování je nejlepší pro rodiny s dětmi.
  Můžete s sebou vzít klidně i tři vaše potomky.
</p>

<p>
  K apartmánu je navíc možné přiobjednat <a href="#room">pokoj</a> za 350 €.
</p>
