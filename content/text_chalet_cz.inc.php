
<h2>Vybavení chaletu</h2>

<ul>
  <li>dva pokoje, celkem pro čtyři osoby</li>
  <li>koupelna s WC a sprchovým koutem</li>
  <li>terasa s krasným vyhledem do kraje</li>
  <li>přístup na zahradu</li>
  <li>společná venkovní kuchyň: otevřená jako altánek, vybavená lednicí, sporákem, mikrovlnkou, rychlovarnou konvicí a myčkou nádobí</li>
</ul>

<p>
K chaletu je možné přiobjednat <a href="#room.php">pokoj</a> za 350 €.
</p>
