<div class="prose">

  <h2>
    <?php echo __('Ceny za celý apartmán') ?>
  </h2>

  <table class="prices">

  <thead>
    <tr>
      <th>
        <?php echo __('Období') ?>
      </th>
      <th class="text-right">
        <?php echo __('Cena / týden') ?>
      </th>
    </tr>
  </thead>

  <tbody>
    <tr>
      <td><?php echo __('duben').' & '.__('říjen')  ?></td>
      <td class="text-right">
          <?php echo show_price('450', $lang) ?>
        </td>
    </tr>
    <tr>
      <td><?php echo __('květen').' & '.__('září') ?></td>
      <td class="text-right">
          <?php echo show_price('550', $lang) ?>
        </td>
    </tr>
    <tr>
      <td><?php echo __('červen') ?> </td>
      <td class="text-right">
        <?php echo show_price('600', $lang) ?>
        </td>
    </tr>
    <tr>
      <td><?php echo __('červenec').' & '.__('srpen') ?></td>
      <td class="text-right">
        <?php echo show_price('700', $lang) ?>
        </td>
    </tr>
  </tbody>
  </table>

</div>

<p>
  <a class="button" target="_blank" href="https://www.google.com/calendar/embed?src=hujgcq1imhcoei99rbkk0cj6a4%40group.calendar.google.com&ctz=Europe/Prague"><?php echo __('Obsazenost') ?></a>
</p>
