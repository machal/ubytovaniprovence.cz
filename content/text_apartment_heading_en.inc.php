<h1>
Apartment Aviotte: two floors, ideal for your family holiday.
</h1>

<p>
  Accommodation in a cosy apartment with garden for up to a family of five.
</p>

<p class="box max-w-md">
  <strong>New for summer 2023: swimming pool 🐬</strong>
  <br>
  A swimming pool with a terrace and countryside views is available from the beginning of June to the end of September.
</p>
