<h2>Chalet equipment</h2>

<ul>
   <li>two rooms, for a total of four people</li>
   <li>bathroom with toilet and shower</li>
   <li>terrace with a beautiful view of the region</li>
   <li>access to the garden</li>
   <li>common outdoor kitchen: open like a gazebo, equipped with fridge, stove, microwave, kettle and dishwasher</li>
</ul>

<p>
It is possible to add a <a href="#room.php">room</a> to the chalet for €350.
</p>
