<h2>
   The apartment is fully equipped
</h2>

<ul>
   <li>own kitchen (fridge, stove, dishwasher, coffee maker)</li>
   <li>living room with library</li>
   <li>master bedroom with large double bed and single bed</li>
   <li>second bedroom with double bed</li>
   <li>bathroom with shower and separate toilet</li>
   <li>separate outdoor sitting area </li>
</ul>

<p>
   The master bedroom is walk-through so the accommodation is best for families with children.
   You can easily take three of your children with you.
</p>

<p>
   In addition, it is possible to order a <a href="#room">room</a> for €350.
</p>
