<h2>
   We are glad to make you happy
</h2>

<figure>
     <blockquote>
       <p>
       “Wonderful evening sitting in front of the house, perfect feeling of privacy
         and distance from the precipitous civilization. We can only recommend a holiday in Apartment Aviotte.”
       </p>
     </blockquote>
     <figcaption>Roman Kratochvíl</figcaption>
</figure>

<figure>
     <blockquote>
       <p>
       “I miss Provence very much. The colors, the lavender, the architecture, the wine and the coziness. We will definitely recommend you.”
       </p>
     </blockquote>
     <figcaption>Gábina Hejhalová</figcaption>
</figure>

<figure>
     <blockquote>
       <p>
       “Staying at the Aviotte Apartment was a mental and physical oasis for us. Environment
            and&nbsp;the accommodation is wonderful and we were definitely satisfied.”
       </p>
     </blockquote>
     <figcaption>Pavel Stejskal with&nbsp;family</figcaption>
</figure>
