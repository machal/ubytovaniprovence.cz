<h1>Accommodation conditions</h1>

<ul>
   <li>Reservations with us are possible without cancellation fees.</li>
   <li>Refundable deposit of €200 on site.</li>
   <li>Stays are from Saturday 13:00 to Saturday 11:00.</li>
   <li>We have 14-day stays with a discount.</li>
   <li>Unfortunately, animals cannot come to us.</li>
   <li>Includes bed linen, bath towels, towels.</li>
   <li>Possibility of changing the conditions individually.</li>
</ul>
