<p class="content__photos-item-full">
  <img src="./images/2022-opt/apartman-obyvak.webp" width="2000" height="1500"
    class="content__photos-img" loading="eager" lightbox="group-apartment" alt="">
</p>

<p class="content__photos-item">
  <img src="./images/2022-opt/apartman-bazen-2.webp" width="1984" height="1488"
    class="content__photos-img" loading="lazy" decoding="async" lightbox="group-apartment" alt="">
</p>

<p class="content__photos-item">
  <img src="./images/2022-opt/apartman-kuchyne.webp" width="2000" height="1500"
    class="content__photos-img" loading="lazy" decoding="async" lightbox="group-apartment" alt="">
</p>

<p class="content__photos-item">
  <img src="./images/2022-opt/apartman-loznice.webp" width="2000" height="1499"
    class="content__photos-img" loading="lazy" decoding="async" lightbox="group-apartment" alt="">
</p>

<p class="content__photos-item">
  <img src="./images/2022-opt/apartman-koupelna.webp" width="1500" height="2000"
    class="content__photos-img" loading="lazy" decoding="async" lightbox="group-apartment" alt="">
</p>

<p class="content__photos-item">
  <img src="./images/2022-opt/apartman-loznice-horni.webp" width="2000" height="1500"
    class="content__photos-img" loading="lazy" decoding="async" lightbox="group-apartment" alt="">
</p>


