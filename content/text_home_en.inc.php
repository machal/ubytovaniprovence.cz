<p>
   Visit beautiful ancient towns, admire the lavender fields, go cycling. Or just get in the car and you’ll be at the sea, in the Savoie Alps or in Avignon in no time!
</p>

<p>
   The <a href="/surroudings.php">surroudings</a> of our accommodation offers a variety of options to relax in beautiful France.
</p>
