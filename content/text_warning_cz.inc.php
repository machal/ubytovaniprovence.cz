<p class="box">
  <strong>Informace k sezóně 2023:</strong><br>
  Vstup do Francie je z hlediska pandemie Covid-19 nyní bez restrikcí.
  Aktuální stav najdete na webu <a href="https://www.mzv.cz/paris/cz/konzularni_informace/aktualne/aktualni_informace_k_cestovani_v.html">Ministerstva zahraničí</a>.
</p>
