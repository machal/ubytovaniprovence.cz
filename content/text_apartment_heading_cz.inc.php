<h1>
  Apartmán Aviotte: dvoupatrové bydlení, ideální pro vaši rodinnou dovolenou.
</h1>

<p>
  Ubytování v útulném apartmánu se zahradou až pro pětičlenou rodinu.
</p>

<p class="box max-w-md">
  <strong>Novinka pro léto 2023: bazén 🐬</strong>
  <br>
  Pro ubytované je k dispozici od začátku června do konce září bazén s terasou a výhledem do kraje.
</p>
