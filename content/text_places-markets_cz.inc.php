<h3>Trhy a kultura</h3>

<p>Byť jste tady na venkově a na místě, kde není zase tak moc turistů, poměrně to zde žije. Určitě se zastavte na některých trzích a nasajte jejich neopakovatelnou atmosféru.</p>

<ul>
  <li><a href="https://www.ladrometourisme.com/en/cards/restaurant-la-fontaine-minerale/">La Fontaine Minerale</a> - sousedské slavnosti (neděle)</li>
<li>Restaurace a trhy Puy-Saint-Martin (pondělí)</li>
<li>Trhy v <a href="https://www.ladrometourisme.com/en/cards/restaurant-la-fontaine-minerale/">Bourdeaux</a> (čtvrtek)</li>
</ul>
