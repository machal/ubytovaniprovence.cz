<h3>Romantic towns</h3>

<p>The charm of the region is completed by small but beautiful towns, where you will not only meet pleasant people, but you will discover something interesting in almost every one.</p>

<ul>
   <li><a href="https://www.ladrometourisme.com/en/cards/puy-saint-martin/">Puy-Saint-Martin</a> (5 minutes by car) - nearest place, possible here shop or visit restaurants.</li>
   <li><a href="https://www.ladrometourisme.com/en/cards/saou/">Saou</a> (10 minutes) – picturesque town, entrance to the Forêt tourist area.</li>
   <li><a href="https://www.ladrometourisme.com/en/cards/crest/">Crest</a> (20 minutes by car) – the largest of the nearby towns, be sure to climb to the viewpoint above the church.</li>
</ul>
