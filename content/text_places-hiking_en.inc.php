<h3>Hiking</h3>

<p>You are under the mountains in the Auvergne-Rhône-Alpes region. You can choose the difficulty of the routes you go on – from walks with children to longer trips. You will remember the views of the colorful fields for a long time.</p>

<ul>
  <li>Walks around, between fields, farms and towns.</li>
  <li><a href="https://www.ladrometourisme.com/en/cards/foret-de-saou/">Forêt de Saou</a> (15 minutes by car) - lowland routes for children, but also proper hikes to mountains over 1,000 meters high. See eg Grande-Pomerole.</li>
  <li><a href="https://www.ladrometourisme.com/en/cards/cirque-darchiane/">Cirque d&#39;Archiane</a> (1.5 hours) - beautiful arched mountains .</li>
  <li>If you prefer rock climbing, it is possible not far from the village of Soau. </li>
</ul>
