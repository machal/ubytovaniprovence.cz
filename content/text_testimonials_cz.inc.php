<h2>
  Těší nás, že vám děláme radost
</h2>

<figure>
    <blockquote>
      <p>
        „Nádherná večerní posezení před domem, perfektní pocit soukromí
        a vzdálení od překotné civilizace. Dovolenou u Vás lze jen doporučit.“
      </p>
    </blockquote>
    <figcaption>Roman Kratochvíl</figcaption>
</figure>

<figure>
    <blockquote>
      <p>
      „Moc mi chybí Provence. Ty barvy, levandule, architektura, víno a ta pohoda. Určitě Vás budeme doporučovat.“
      </p>
    </blockquote>
    <figcaption>Gábina Hejhalová</figcaption>
</figure>

<figure>
    <blockquote>
      <p>
      „Pobyt u&nbsp;vás byl pro nás duševní i&nbsp;fyzickou oázou. Prostředí
           i&nbsp;ubytování je nádherné a rozhodně jsme byli velmi spokojeni.“
      </p>
    </blockquote>
    <figcaption>Pavel Stejskal s&nbsp;rodinou</figcaption>
</figure>


