<h1>
  Okolí apartmánu: Vítejte v Auvergne-Rhône-Alpes, hornaté oblasti mezi řekou Rhônou, Alpami a Provence.
</h1>

<p>Naše ubytování najdete v osadě Saudon, která spadá pod obec <a href="https://www.ladrometourisme.com/en/cards/soyans/">Soyans</a>.  </p>

