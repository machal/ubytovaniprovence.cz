<p>
  Navštivte nádherná starobylá městečka, kochejte se levandulovými poli, vyražte na kole. A nebo si prostě sedněte do auta a za chvíli jste u moře, v Savojských Alpách či v Avignonu!
</p>

<p>
  <a href="/surroudings.php">Okolí</a> našeho ubytování nabízí celou řadu možností, jak si odpočinout v krásné Francii.
</p>

