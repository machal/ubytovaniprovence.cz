<h1>
  Chalet Aviotte: dvoupokojový chalet pro čtyři osoby.
</h1>

<p>
  Soukromí pro všechny a terasa s nádherným výhledem k tomu. Chalet je dvoupokojový apartmán s možností využít společnou venkovní kuchyň a posezení.
</p>
