<h2>Co u nás můžete podnikat?</h2>

<ul>
  <li>Pěší turistika (malebná městečka nebo vyšší kopce).</li>
  <li>Cyklistika (pokud máte rádi kopečky).</li>
  <li>Lezení po skalách.</li>
  <li>Koupání (řeka, bazény…).</li>
  <li>Sjíždění řek (kanoe, kajak na krásné řece Drómě). </li>
</ul>
