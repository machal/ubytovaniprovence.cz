<div class="prose">

<h2>
  <?php echo __('Ceny za pokoj se snídaní') ?>
</h2>


<table class="prices">
<thead>
  <tr>
    <th>
      <?php echo __('Období') ?>
    </th>
    <th class="text-right">
      <?php echo __('Cena / týden') ?>
    </th>
  </tr>
</thead>
 <tbody>
   <tr>
     <td><?php echo __('červenec').' & '.__('srpen') ?></td>
     <td class="text-right">
       <?php echo show_price('450', $lang) ?>
      </td>
   </tr>
 </tbody>
</table>

</div>

<p class="center">
  <a class="button" target="_blank" href="https://www.google.com/calendar/embed?src=sciogvqs4ci3jmdoh0les35a50%40group.calendar.google.com&ctz=Europe/Prague"><?php echo __('Obsazenost') ?></a>
</p>
