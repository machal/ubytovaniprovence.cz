<h1>
   Chalet Aviotte: two-room chalet for four people.
</h1>

<p>
   Privacy for all and a terrace with a wonderful view to it. Chalet is a two-room apartment with the possibility to use a common outdoor kitchen and sitting area.
</p>
