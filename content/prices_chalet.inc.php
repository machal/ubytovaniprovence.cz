<div class="prose">

<h2>
  <?php echo __('Ceny za celý chalet') ?>
</h2>

<table class="prices">
<thead>
  <tr>
    <th>
      <?php echo __('Období') ?>
    </th>
    <th class="text-right">
      <?php echo __('Cena / týden') ?>
    </th>
  </tr>
</thead>
<tbody>
   <tr>
     <td><?php echo __('červen').' & '.__('září') ?></td>
     <td class="text-right">
       <?php echo show_price('550', $lang) ?>
      </td>
   </tr>
   <tr>
     <td><?php echo __('červenec').' & '.__('srpen') ?></td>
     <td class="text-right">
        <?php echo show_price('650', $lang) ?>
      </td>
   </tr>
 </tbody>
</table>

</div>

<p class="center">
  <a class="button" target="_blank" href="https://www.google.com/calendar/embed?src=hqi8uv249kvne9bmdean01osp4%40group.calendar.google.com&ctz=Europe/Prague"><?php echo __('Obsazenost') ?></a>
</p>
