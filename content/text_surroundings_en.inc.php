<h2>What can you do with us?</h2>

<ul>
   <li>Hiking (picturesque towns or higher hills).</li>
   <li>Cycling (if you like hills).</li>
   <li>Rock climbing.</li>
   <li>Swimming (river, pools...).</li>
   <li>River rafting (canoeing, kayaking on the beautiful Dróma River). </li>
</ul>
