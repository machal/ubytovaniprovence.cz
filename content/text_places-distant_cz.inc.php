<h2>Vzdálenější města</h2>

<p>Sedněte do auta a udělejte si výlet. Za historií do Avignonu nebo za mořem k Marseille.  </p>

<ul>
  <li><a href="https://cs.wikipedia.org/wiki/Orange_(Francie)">Orange</a> (45 minut autem)</li>
  <li><a href="https://cs.wikipedia.org/wiki/Pont_du_Gard">Pont du Gard</a>, <a href="https://cs.wikipedia.org/wiki/Avignon">Avignon</a> (hodina)</li>
  <li><a href="https://cs.wikipedia.org/wiki/Arles">Arles</a> (1,5 hodiny)</li>
  <li><a href="https://cs.wikipedia.org/wiki/Marseille">Marseille</a> a moře (2 hodiny)  </li>
</ul>
