<h1>
   Surroundings of the apartment: Welcome to Auvergne-Rhône-Alpes, a mountainous region between the Rhône River, the Alps and Provence.
</h1>

<p>You can find our accommodation in the village of Saudon, which falls under the municipality of <a href="https://www.ladrometourisme.com/en/cards/soyans/">Soyans</a>. </p>
