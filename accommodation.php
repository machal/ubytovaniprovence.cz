<?php

require_once '_init.inc.php';

$title = __('Apartmán v Provence');
$description = __('Ubytování Aviotte je ideální pro rodinnou dovolenou nebo pobyty pro páry.');
$image = '/images/2022-opt/venku-komplet.jpeg';

require_once '_head.inc.php';

?>

<div class="container lg:max-w-[1200px]">

      <div class="nav" role="navigation">
        <h2 class="sr-only">Navigace</h2>
        <ul class="nav__wrapper">
          <li class="nav__item">
            <a class="nav__item-inside" href="/"><?php echo __('Úvod')  ?></a>
          </li>
          <li class="nav__item">
            <span class="nav__item-inside"><?php echo __('Ubytování')  ?></span>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="surroundings.php"><?php echo __('Okolí')  ?></a>
          </li>
          <li class="nav__item">
            <a class="nav__item-inside" href="contact.php"><?php echo __('Kontakt')  ?></a>
          </li>
        </ul>
      </div><!-- #nav -->

      <div class="content-container">

      <?php
      require_once '_offer_bar.inc.php';
      ?>

      <div class="content">

        <!-- Apartman -->

        <div id="apartment" class="content__heading">
          <?php require_once('content/text_apartment_heading_'.$lang.'.inc.php');  ?>
        </div>

        <div class="content__photos">
          <?php require_once('content/photos_apartment.inc.php');  ?>
        </div>

        <div class="md:grid md:grid-cols-[2fr_1fr] md:gap-8">
          <div class="prose">
            <?php require_once('content/text_apartment_'.$lang.'.inc.php');  ?>
          </div>
          <div>
            <?php require_once('content/prices_apartment.inc.php');  ?>
          </div>
        </div>

        <hr class="divider">

        <!-- Chalet -->

        <div id="chalet" class="content__heading">
          <?php require_once('content/text_chalet_heading_'.$lang.'.inc.php');  ?>
        </div>

        <div class="content__photos">
          <?php require_once('content/photos_chalet.inc.php');  ?>
        </div>

        <div class="md:grid md:grid-cols-[2fr_1fr] md:gap-8">
          <div class="prose">
            <?php require_once('content/text_chalet_'.$lang.'.inc.php');  ?>
          </div>
          <div>
            <?php require_once('content/prices_chalet.inc.php');  ?>
          </div>
        </div>

        <hr class="divider">

        <!-- Pokoj -->

        <div id="room" class="content__heading">
          <?php require_once('content/text_room_heading_'.$lang.'.inc.php');  ?>
        </div>

        <div class="content__photos">
          <?php require_once('content/photos_room.inc.php');  ?>
        </div>

        <div class="md:grid md:grid-cols-[2fr_1fr] md:gap-8">
          <div class="prose">
            <?php require_once('content/text_room_'.$lang.'.inc.php');  ?>
          </div>
          <div>
            <?php require_once('content/prices_room.inc.php');  ?>
          </div>
        </div>

        <hr class="divider">

        <!-- Podminky -->

        <div id="terms" class="prose">
          <?php require_once('content/text_terms_'.$lang.'.inc.php');  ?>
        </div>

      </div><!-- .content -->

      </div><!-- .content-container -->

<?php
require_once '_page_foot.inc.php';
?>

  </div><!-- .container -->

<?php
require_once '_foot.inc.php';
?>
