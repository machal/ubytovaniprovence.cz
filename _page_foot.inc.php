<div class="foot" role="contentinfo">

  <p>
    <strong><?php echo __('Objednejte hned, domluvíte se s námi česky!') ?></strong>
    <br>
    <br class="sm:hidden">
    <a href="mailto:zuzanaviotte@gmail.com">zuzanaviotte@gmail.com</a>
    <span class="hidden sm:inline">–</span>
    <br class="sm:hidden">
    T <a href="tel:+33647562355">+33 647 562 355</a>
    <span class="hidden sm:inline">–</span>
    <br class="sm:hidden">
    <a  target="_blank" href="https://www.facebook.com/AviotteProvence">Facebook</a>
  </p>

</div>

<p class="lang">
  <?php if ($lang == 'cz'):  ?>
    <span class="item">Česky</span> /
  <?php else: ?>
    <a class="item" href="https://<?= $domain['cz'][$env].$_SERVER['REQUEST_URI'] ?>">Česky</a> /
  <?php endif; ?>
  <?php if ($lang == 'en'):  ?>
    <span class="item">English</span>
  <?php else: ?>
    <a class="item" href="https://<?= $domain['en'][$env].$_SERVER['REQUEST_URI'] ?>">English</a>
  <?php endif; ?>
  <?php /*
  /
  <?php if ($lang == 'de'):  ?>
    <span class="item">Deutsch</span>
  <?php else: ?>
    <a class="item" href="https://<?= $domain['de'][$env].$_SERVER['REQUEST_URI'] ?>">Deutsch</a>
  <?php endif; ?>
  */ ?>
</p>
