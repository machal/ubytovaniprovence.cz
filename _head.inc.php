<!DOCTYPE html>

<html lang="<?php echo ($lang == 'cz') ? 'cs' : $lang ?>">
<head>
  <meta charset="UTF-8">

  <title><?php echo $title ?></title>

  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- Kriticke zdroje: -->
  <link rel="stylesheet" href="/dist/style.css?2">

  <!-- Google Fonts potrebujeme brzy: -->
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">

  <!-- Bento, hlavne kvuli Lightboxu: -->
  <script type="module" async src="https://cdn.ampproject.org/bento.mjs"></script>
  <script nomodule src="https://cdn.ampproject.org/bento.js"></script>
  <link rel="stylesheet" type="text/css" href="https://cdn.ampproject.org/v0/bento-lightbox-gallery-1.0.css">
  <script type="module" async src="https://cdn.ampproject.org/v0/bento-lightbox-gallery-1.0.mjs"></script>
  <script nomodule async src="https://cdn.ampproject.org/v0/bento-lightbox-gallery-1.0.js"></script>

  <?php /*
  <!-- Google Analytics -->

  <script>
  window.ga=window.ga||function(){(ga.q=ga.q||[]).push(arguments)};ga.l=+new Date;
  ga('create', 'UA-568668-2', 'auto');
  ga('send', 'pageview');
  </script>
  <script async src='https://www.google-analytics.com/analytics.js'></script>
  <!-- End Google Analytics -->
  */
  ?>

  <meta property="og:title" content="<?php echo $title ?>">
  <meta property="og:description" content="<?php echo $description ?>">
  <meta property="og:image" content="<?php echo $image ?>">
  <meta name="description" content="<?php echo $description ?>">
  <meta name="author" content="Martin Michálek; www.vzhurudolu.cz/martin" />

  <!-- Favicon -->
  <link rel="apple-touch-icon" sizes="152x152" href="/apple-touch-icon.png">
  <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
  <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
  <link rel="manifest" href="/site.webmanifest">
  <link rel="mask-icon" href="/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="msapplication-TileColor" content="#da532c">
  <meta name="theme-color" content="#ffffff">

</head>

<?php
// body nebo .scheme-blue - barevne schema pro ozvlastneni
?>

<body class="">

<?php
// Bento Lightbox Gallery https://bentojs.dev/components/bento-lightbox-gallery/
?>
  <bento-lightbox-gallery></bento-lightbox-gallery>
