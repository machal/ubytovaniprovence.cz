<?php

function show_price($price, $lang) {
  if ($lang == 'cz') {
    return '<a target="_blank" href="https://www.google.com/search?q='.$price.'+EUR+to+CZK" title="Klikněte pro přibližný převod z Eur do Kč, přesný najdete na www.cnb.cz">'.$price.'&nbsp;€</a>';
  } else {
    return $price.'&nbsp;€';
  }
}

?>
