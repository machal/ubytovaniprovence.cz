<?php

$phrases = array(
  'test'      => array(
    'en' => 'test EN',
    'de' => 'test DE'
  ),
  'Úvod'                 => array(
    'en' => 'Home',
    'de' => 'Home'
  ),
  'Ubytování'           => array(
    'en' => 'Accommodation',
    'de' => 'Unterkunft'
  ),
  'Pokoj'           => array(
    'en' => 'Room',
    'de' => 'Zimmer'
  ),
  'Apartmán'           => array(
    'en' => 'Apartment',
    'de' => 'Appartement'
  ),
  'Chalet'           => array(
    'en' => 'Chalet',
    'de' => 'Chalet'
  ),
  'Okolí'        => array(
    'en' => 'Location',
    'de' => 'Umgebung'
  ),
  'Kontakt'        => array(
    'en' => 'Contact',
    'de' => 'Kontakt'
  ),
  'Apartmán v Provence'                 => array(
    'en' => 'Apartment in Provence',
    'de' => 'Apartment in der Provence'
  ),
  'Drôme Provençale'                 => array(
    'en' => 'Drôme Provençale',
    'de' => 'Drôme Provençale'
  ),
  'Aviotte - Ubytování v Provence, Francie'                 => array(
    'en' => 'Aviotte - Accommodation in Provence, France',
    'de' => 'Aviotte - Unterkunft in der Provence, Frankreich'
  ),
  'Ubytování v Provence – apartmán a pokoje'                 => array(
    'en' => 'Accommodation in Provence – apartment and rooms',
    'de' => 'Unterkunft in der Provence – Apartment und Zimmer'
  ),
  'Pokoj v Provence'                 => array(
    'en' => 'Room in Provence',
    'de' => 'Zimmer in der Provence'
  ),
  'Apartmán v Provence'                 => array(
    'en' => 'Apartment in Provence',
    'de' => 'Appartement in der Provence'
  ),
  'Objednejte hned, domluvíte se s námi česky!'                 => array(
    'en' => '',
    'de' => ''
  ),
  'Obsazenost'                 => array(
    'en' => 'Availability',
    'de' => 'Verfügbarkeit'
  ),
  'Mapa a GPS'                 => array(
    'en' => 'Map and GPS',
    'de' => 'Karte und GPS'
  ),
  'Kontakt:'                 => array(
    'en' => 'Contact:',
    'de' => 'Kontakt:'
  ),
  'další kontakty'                 => array(
    'en' => 'full contacts',
    'de' => 'Alle Kontakte'
  ),
  'Mapa okolí na Google Maps' => array(
    'en' => 'Area Map on Google Maps',
    'de' => 'Umgebung auf Google Maps'
  ),
  'Fotogalerie na Flickr.com'                 => array(
    'en' => 'Flickr photoset',
    'de' => 'Flickr Fotogalerie'
  ),
  'Ceník za celý apartmán a týden' => array(
    'en' => 'Price per apartment and week',
    'de' => 'Preis pro Ap. und Woche'
  ),
  'Ceník za pokoj se snídaní na týden' => array(
    'en' => 'Prices for a week with breakfast',
    'de' => 'Preis pro Ap. und Woche mit Frühstück'
  ),
  'duben' => array(
    'en' => 'April',
    'de' => 'April'
  ),
  'květen' => array(
    'en' => 'May',
    'de' => 'Mai'
  ),
  'červen' => array(
    'en' => 'Juny',
    'de' => 'Juni'
  ),
  'červenec' => array(
    'en' => 'July',
    'de' => 'Juli'
  ),
  'srpen' => array(
    'en' => 'August',
    'de' => 'August'
  ),
  'září' => array(
    'en' => 'September',
    'de' => 'September'
  ),
  'říjen' => array(
    'en' => 'October',
    'de' => 'Oktober'
  ),
  'Vítejte v Drôme Provençale, hornaté oblasti mezi řekou Rhônou a&nbsp;Alpami.' => array(
    'en' => 'Welcome to Drôme Provençale mountain landscape between river Rhone and the Alps.',
    'de' => 'Willkommen in Drome Provencale Berglandschaft zwischen dem Fluß Rhone und den Alpen.'
  ),
  'Apartmán Aviotte. Vaše klidná dovolená mezi levandulovými&nbsp;poli.' => array(
    'en' => 'Apartment Aviotte. Your quiet place at the lavender fields.',
    'de' => 'Apartment Aviotte. Ihr ruhiger Urlaub an den Lavendelfeldern.'
  ),
  'Apartmán Aviotte je ideální pro rodinnou dovolenou. 2 dospělí s&nbsp;sebou mohou vzít klidně i 3 děti.' => array(
    'en' => 'Apartment Aviotte perfect for familietrips. Two adults can take up to 3 children with them. For the kids we have a small pool.',
    'de' => 'Apartment Aviotte ist für einen Familienurlaub ideal, 2 Erwachsene können problemlos  3 Kinder mitnehmen.'
  ),
  'Ceny za celý apartmán'                 => array(
    'en' => 'Prices for the apartment',
    'de' => 'Preise für die Wohnung'
  ),
  'Období'                 => array(
    'en' => 'The season',
    'de' => ''
  ),
  'Cena / týden'                 => array(
    'en' => 'Price / week',
    'de' => ''
  ),
  'Ubytování Aviotte je ideální pro rodinnou dovolenou nebo pobyty pro páry.'                 => array(
    'en' => 'The Aviotte accommodation is ideal for family holidays or stays for couples.',
    'de' => ''
  ),
  'Ceny za celý chalet'                 => array(
    'en' => 'Prices for the whole chalet',
    'de' => ''
  ),
  'Ceny za pokoj se snídaní'                 => array(
    'en' => 'Prices with breakfast',
    'de' => ''
  ),
  'Okolí Apartmánu'                 => array(
    'en' => 'Surroundings of the apartment',
    'de' => ''
  ),
);
?>
