<?php

// --- Jazyk a prostredi ---

$lang = 'cz';
$env = 'development';

$domain['cz']['development'] = 'www.upcz.loc:8890';
$domain['en']['development'] = 'www.upcom.loc:8890';
$domain['de']['development'] = 'www.upde.loc:8890';
$domain['cz']['production'] = 'www.ubytovani-provence.cz';
$domain['en']['production'] = 'www.aviotte-provence.com';
$domain['de']['production'] = 'www.unterkunft-provence.de';

switch ($_SERVER['HTTP_HOST']) {
  case $domain['cz']['development']:
    $lang = 'cz';
    $env = 'development';
    break;
  case $domain['en']['development']:
    $lang = 'en';
    $env = 'development';
    break;
  case $domain['de']['development']:
    $lang = 'de';
    $env = 'development';
    break;
  case $domain['cz']['production']:
    $lang = 'cz';
    $env = 'production';
    break;
  case $domain['en']['production']:
    $lang = 'en';
    $env = 'production';
    break;
  case $domain['de']['production']:
    $lang = 'de';
    $env = 'production';
    break;
}

// Debug
//echo $lang;
//echo '<br>';
//echo $env;

require_once 'inc/helpers.inc.php';

require_once 'inc/dictionary.inc.php';

function __($phrase) {
  global $phrases;
  global $lang;
  if ($lang == 'cz') {
    return $phrase;
  } else {
    return $phrases[$phrase][$lang];
  }
}



// --- Mobile Detect ---

include 'inc/Mobile_Detect.php';
$detect = new Mobile_Detect();

// iOS/Android
$is_ios_android = ($detect->isiOS() || $detect->isAndroidOS());

// Krapo-phone
$is_dumbphone = ($detect->isMobile() && !$detect->isTablet() && !$is_ios_android);

?>
