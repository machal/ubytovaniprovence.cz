<?php /*

<div class="offer-bar">

<?php if ($lang == 'cz'): ?>

  <p>
    <strong>
      Speciální nabídka:
    </strong>
    <a href="/accommodation.phhp" class="underline">Chalet</a>
    – dva pokoje pro dvě osoby za 500 €!
    <br class="sm:hidden">
    ➔
    <span class="text-suppress">
      <a href="mailto:zuzanaviotte@gmail.com" class="underline">zuzanaviotte@gmail.com</a>
    </span>
  </p>

<?php elseif ($lang == 'en'): ?>

  <p>
    <strong>
    Special offer:
    </strong>
    <a href="/accommodation.phhp" class="underline">Chalet</a>
    - two rooms for two people for 500 €!
    ➔
    <span class="text-suppress">
      <a href="mailto:zuzanaviotte@gmail.com" class="underline">zuzanaviotte@gmail.com</a>
    </span>
  </p>

<?php elseif ($lang == 'de'): ?>

  <p>
    <strong>
    Sonderangebot:
    </strong>
    <a href="/accommodation.phhp" class="underline">Chalet</a>
    – zwei Zimmer für zwei Personen für 500 €!
    ➔
    <span class="text-suppress">
      <a href="mailto:zuzanaviotte@gmail.com" class="underline">zuzanaviotte@gmail.com</a>
    </span>
  </p>

<?php endif; ?>

</div>

*/ ?>
